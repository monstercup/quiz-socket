const express = require('express');
const app = express();
const http = require('http').Server(app);
const PORT = 4000;
const cors = require('cors');
const NodeCache = require( "node-cache" );
const myCache = new NodeCache({ checkperiod: 60 })

app.use(cors());
app.use(express.json());

const io = require('socket.io')(http, {
    cors: {
        origin: "http://localhost:3000"
    }
});

app.use(function(req, res, next) {
  res.setHeader('Content-Type', 'application/json');
  next();
});

io.on('connection', (socket) => {
    socket.on("create-game", (game) => {
      socket.join(["game", "admin"]);
    })
    socket.on("join-game", (game) => {
      socket.join("game");
      io.to('admin').emit('game-update', game)
    })

    socket.on("start-game", () => {
      io.to(["game", 'admin']).emit("start")
    })

    socket.on("buzz", (player) => {
      // add to queue
      let game = myCache.get( "game" )
      const playerIndex = game.players.findIndex(p => p.name === player.name)
      game.players[playerIndex] = {name:player.name, score:player.score, canBuzz:false}
      myCache.set( 'game', game, 10000)
      io.to("game").emit("new-buzz", game.players[playerIndex])
      socket.emit("player-update", game.players[playerIndex])
    })
    socket.on('queue', (queue) => {
      socket.emit("queue_update", queue)
    })

    socket.on("cancel-game", () => {
      myCache.del( "game" );
      io.to("game").emit('cancelled')
      io.in(["game", 'admin']).socketsLeave(["game", "admin"]);
    })

    socket.on('update_score', (player, game) => {
      let filtered_players = game.players.filter(v => v.name !== player.name)
      let active_player = {name:player.name, score:player.score, canBuzz:false}
      filtered_players.push(active_player)
      let new_game = {id:game.id, players: filtered_players, status:game.status,}
      io.to('game').emit("game-update", new_game)
      myCache.set( 'game', new_game, 10000)
    })

    socket.on('queue', (queue) => {
      io.to('game').emit('update_queue', queue)
    })

    socket.on('end-game', (game) => {
      io.to('game').emit('end', game)
    })

    socket.on('reset_buzzers', game => {
      let filtered_players = game.players.map(v => { return {...v, canBuzz:true}} )
      let new_game = {id:game.id, players: filtered_players, status:game.status}
      io.to('game').emit("game-update", new_game)
      myCache.set( 'game', new_game, 10000)     
    })

    myCache.on( "expired", function( key, value ){
      io.to("game").emit('cancelled')
      io.in(["game", 'admin']).socketsLeave(["game", "admin"]);
    });

    myCache.on( "del", function( key, value ){
      io.to("game").emit('cancelled')
      io.in(["game", 'admin']).socketsLeave(["game", "admin"]);
    });

    // myCache.on( "set", function( key, value ){
    //   if(key === "game") socket.emit("game-update", value)
    //   console.log("set game")
    //   socket.join("game");
    // });
    
});

io.on('disconnect', (socket) => {
  socket.on('leave-game', (gameId, playerName) => {
    let game = myCache.get( "game" )
    if (game && game.id == gameId){
      game.players.filter(el => {
        return el.name !== playerName
    })
    }
  })
})

// GAME


app.post('/game/new', (req, res) => {
  myCache.set("game", req.body, 10000)
  res.json(req.body)
})

// checking if a game is ongoing and if the id stored in local storage matches the current game id
app.get('/game/:id', (req, res) => {
  let game = myCache.get( "game" )
  if (game && game.id == req.params.id){
    res.json(game)
  }
  // else {
  //   {res.status(400)
  //   res.json("Pas de partie avec cet identifiant" )}
  // }
})

app.put('/game/:id/edit', (req, res) => {
  // TODO
  res.json("!")
})

app.delete('/game/:id/delete', (req, res) => {
    // TODO
    res.json("!")
})

// join game, using a correct id, and registering the player in the process
app.post('/join-game', (req, res) => {
  let game = myCache.get( "game" )
  if (game && game.id == req.body.gameId){
    game.players.push({name:req.body.name, score:0, canBuzz:true})
    myCache.set( 'game', game, 10000)
    res.json(game)
    io.on('connection', (socket) => {
      socket.emit("game-update", game)
      socket.join("game");
    }
  )}
  else {
    res.status(400)
    res.json("Mauvais identifiant" )}
})

app.post('/leave-game', (req, res) => {
  let game = myCache.get( "game" )
  if (game && game.id == req.body.gameId){
    let filtered_players = game.players.filter(v => v.name !== req.body.name)
    myCache.set( 'game', {id: game.id, players:filtered_players, status:game.status }, 10000)
    res.json(game)
    io.on('connection', (socket) => {
      socket.emit("game-update", game)
      socket.leave("game");
    }
  )}
  else {
    res.status(400)
    res.json("Mauvais identifiant" )}
})

// DEFAULT

// app.post('/', (req, res) => {
//   res.json(req.body);
// });

// app.get('/',function(req,res) {
//     res.send('Socket app')
//   });

http.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});